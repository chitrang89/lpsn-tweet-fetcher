
var fs = require('fs'),
    path = require('path'),
    Sequelize = require('sequelize'),
    _ = require('lodash');

var sequelize = new Sequelize('LivePerson-TweetStorage', null, null, {
        dialect: 'sqlite',
        storage: 'data.db'
    }),
    db = {};
var SearchObject = sequelize.define('SearchObjects', {
    TweetsObject: {
        type: Sequelize.STRING
    },
    TweetId:{
        type: Sequelize.STRING
    }
});
db['SearchObject'] = SearchObject;

Object.keys(db).forEach( function(modelName) {
    if( 'associate' in db[modelName] ) {
        db[modelName].associate(db);
    }
});
var response = {
  "statuses": [
    {
      "created_at": "Tue Jun 13 01:51:56 +0000 2017",
      "id": 874444190968668200,
      "id_str": "874444190968668160",
      "text": "RT @actioningeorgia: Partnering with #FeedingATL &amp; #LivePerson to build 1,517 food boxes for Atlanta's food-insecure families. 53,068 lbs o…",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "FeedingATL",
            "indices": [
              37,
              48
            ]
          },
          {
            "text": "LivePerson",
            "indices": [
              55,
              66
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "actioningeorgia",
            "name": "Action Ministries",
            "id": 318459895,
            "id_str": "318459895",
            "indices": [
              3,
              19
            ]
          }
        ],
        "urls": []
      },
      "metadata": {
        "iso_language_code": "en",
        "result_type": "recent"
      },
      "source": "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 3041944074,
        "id_str": "3041944074",
        "name": "Lisa Gill",
        "screen_name": "lisatgill13",
        "location": "Dunwoody, GA",
        "description": "Independent Contractor #marketing #projectmanagement #nonprofit @pebbletossers",
        "url": "https://t.co/TpnmchJBCN",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "https://t.co/TpnmchJBCN",
                "expanded_url": "https://www.linkedin.com/in/lisa-gill-a82705b0/",
                "display_url": "linkedin.com/in/lisa-gill-a…",
                "indices": [
                  0,
                  23
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 4,
        "friends_count": 44,
        "listed_count": 0,
        "created_at": "Thu Feb 26 01:22:30 +0000 2015",
        "favourites_count": 18,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 46,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "000000",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/865263940753702912/7scUDW4p_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/865263940753702912/7scUDW4p_normal.jpg",
        "profile_link_color": "1B95E0",
        "profile_sidebar_border_color": "000000",
        "profile_sidebar_fill_color": "000000",
        "profile_text_color": "000000",
        "profile_use_background_image": false,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "retweeted_status": {
        "created_at": "Mon Jun 12 21:18:04 +0000 2017",
        "id": 874375269964021800,
        "id_str": "874375269964021760",
        "text": "Partnering with #FeedingATL &amp; #LivePerson to build 1,517 food boxes for Atlanta's food-insecure families. 53,068 lb… https://t.co/yL2sNbPTh4",
        "truncated": true,
        "entities": {
          "hashtags": [
            {
              "text": "FeedingATL",
              "indices": [
                16,
                27
              ]
            },
            {
              "text": "LivePerson",
              "indices": [
                34,
                45
              ]
            }
          ],
          "symbols": [],
          "user_mentions": [],
          "urls": [
            {
              "url": "https://t.co/yL2sNbPTh4",
              "expanded_url": "https://twitter.com/i/web/status/874375269964021760",
              "display_url": "twitter.com/i/web/status/8…",
              "indices": [
                121,
                144
              ]
            }
          ]
        },
        "metadata": {
          "iso_language_code": "en",
          "result_type": "recent"
        },
        "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "user": {
          "id": 318459895,
          "id_str": "318459895",
          "name": "Action Ministries",
          "screen_name": "actioningeorgia",
          "location": "Georgia",
          "description": "Action Ministries mobilizes communities to address the challenges of poverty by focusing on hunger relief, housing and education.",
          "url": "http://t.co/Nyz28ot6ab",
          "entities": {
            "url": {
              "urls": [
                {
                  "url": "http://t.co/Nyz28ot6ab",
                  "expanded_url": "http://actionministries.net",
                  "display_url": "actionministries.net",
                  "indices": [
                    0,
                    22
                  ]
                }
              ]
            },
            "description": {
              "urls": []
            }
          },
          "protected": false,
          "followers_count": 730,
          "friends_count": 822,
          "listed_count": 23,
          "created_at": "Thu Jun 16 14:38:30 +0000 2011",
          "favourites_count": 214,
          "utc_offset": -14400,
          "time_zone": "Eastern Time (US & Canada)",
          "geo_enabled": true,
          "verified": false,
          "statuses_count": 1764,
          "lang": "en",
          "contributors_enabled": false,
          "is_translator": false,
          "is_translation_enabled": false,
          "profile_background_color": "FFF04D",
          "profile_background_image_url": "http://abs.twimg.com/images/themes/theme19/bg.gif",
          "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme19/bg.gif",
          "profile_background_tile": false,
          "profile_image_url": "http://pbs.twimg.com/profile_images/1702911269/AMI_-_test_Twitter_normal.jpg",
          "profile_image_url_https": "https://pbs.twimg.com/profile_images/1702911269/AMI_-_test_Twitter_normal.jpg",
          "profile_banner_url": "https://pbs.twimg.com/profile_banners/318459895/1415631329",
          "profile_link_color": "0099CC",
          "profile_sidebar_border_color": "FFF8AD",
          "profile_sidebar_fill_color": "F6FFD1",
          "profile_text_color": "333333",
          "profile_use_background_image": true,
          "has_extended_profile": false,
          "default_profile": false,
          "default_profile_image": false,
          "following": false,
          "follow_request_sent": false,
          "notifications": false,
          "translator_type": "none"
        },
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "is_quote_status": false,
        "retweet_count": 1,
        "favorite_count": 1,
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
      },
      "is_quote_status": false,
      "retweet_count": 1,
      "favorite_count": 0,
      "favorited": false,
      "retweeted": false,
      "lang": "en"
    },
    {
      "created_at": "Mon Jun 12 21:18:04 +0000 2017",
      "id": 874375269964021800,
      "id_str": "874375269964021760",
      "text": "Partnering with #FeedingATL &amp; #LivePerson to build 1,517 food boxes for Atlanta's food-insecure families. 53,068 lb… https://t.co/yL2sNbPTh4",
      "truncated": true,
      "entities": {
        "hashtags": [
          {
            "text": "FeedingATL",
            "indices": [
              16,
              27
            ]
          },
          {
            "text": "LivePerson",
            "indices": [
              34,
              45
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [],
        "urls": [
          {
            "url": "https://t.co/yL2sNbPTh4",
            "expanded_url": "https://twitter.com/i/web/status/874375269964021760",
            "display_url": "twitter.com/i/web/status/8…",
            "indices": [
              121,
              144
            ]
          }
        ]
      },
      "metadata": {
        "iso_language_code": "en",
        "result_type": "recent"
      },
      "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 318459895,
        "id_str": "318459895",
        "name": "Action Ministries",
        "screen_name": "actioningeorgia",
        "location": "Georgia",
        "description": "Action Ministries mobilizes communities to address the challenges of poverty by focusing on hunger relief, housing and education.",
        "url": "http://t.co/Nyz28ot6ab",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/Nyz28ot6ab",
                "expanded_url": "http://actionministries.net",
                "display_url": "actionministries.net",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 730,
        "friends_count": 822,
        "listed_count": 23,
        "created_at": "Thu Jun 16 14:38:30 +0000 2011",
        "favourites_count": 214,
        "utc_offset": -14400,
        "time_zone": "Eastern Time (US & Canada)",
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 1764,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "FFF04D",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme19/bg.gif",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme19/bg.gif",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/1702911269/AMI_-_test_Twitter_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/1702911269/AMI_-_test_Twitter_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/318459895/1415631329",
        "profile_link_color": "0099CC",
        "profile_sidebar_border_color": "FFF8AD",
        "profile_sidebar_fill_color": "F6FFD1",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 1,
      "favorite_count": 1,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Mon Jun 12 17:04:08 +0000 2017",
      "id": 874311367431344100,
      "id_str": "874311367431344128",
      "text": "#artifitialintelligence #IBMWatson #LivePerson powering next gen #Vodafoneuk #chatbot  #myvodadone https://t.co/vPut4v2Fhv via @VodafoneUK",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "artifitialintelligence",
            "indices": [
              0,
              23
            ]
          },
          {
            "text": "IBMWatson",
            "indices": [
              24,
              34
            ]
          },
          {
            "text": "LivePerson",
            "indices": [
              35,
              46
            ]
          },
          {
            "text": "Vodafoneuk",
            "indices": [
              65,
              76
            ]
          },
          {
            "text": "chatbot",
            "indices": [
              77,
              85
            ]
          },
          {
            "text": "myvodadone",
            "indices": [
              87,
              98
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "VodafoneUK",
            "name": "Vodafone UK",
            "id": 20678384,
            "id_str": "20678384",
            "indices": [
              127,
              138
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/vPut4v2Fhv",
            "expanded_url": "https://blog.vodafone.co.uk/2017/04/12/meet-tobi-chatbot-latest-addition-vodafone-uks-customer-service-team/",
            "display_url": "blog.vodafone.co.uk/2017/04/12/mee…",
            "indices": [
              99,
              122
            ]
          }
        ]
      },
      "metadata": {
        "iso_language_code": "en",
        "result_type": "recent"
      },
      "source": "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 88183455,
        "id_str": "88183455",
        "name": "Anuj Vyas",
        "screen_name": "anuj_vyas",
        "location": "India",
        "description": "Digital Consultant @BTIC Atos, Loves Bike Riding, Technology Evangelist, Nature Lover and Fan of Game of Throne . Views are my own.",
        "url": "https://t.co/FYw0Wlv0UN",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "https://t.co/FYw0Wlv0UN",
                "expanded_url": "http://www.linkedin.com/in/anuj-vyas",
                "display_url": "linkedin.com/in/anuj-vyas",
                "indices": [
                  0,
                  23
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 291,
        "friends_count": 279,
        "listed_count": 72,
        "created_at": "Sat Nov 07 13:14:56 +0000 2009",
        "favourites_count": 97,
        "utc_offset": 19800,
        "time_zone": "Mumbai",
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 3664,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "510800",
        "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/328086990/x23ebdc6603861ef92b64c2f4adb84d3.jpg",
        "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/328086990/x23ebdc6603861ef92b64c2f4adb84d3.jpg",
        "profile_background_tile": true,
        "profile_image_url": "http://pbs.twimg.com/profile_images/378800000122305549/819ca8363e920c36215f847ae72a914c_normal.jpeg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/378800000122305549/819ca8363e920c36215f847ae72a914c_normal.jpeg",
        "profile_link_color": "2A42DE",
        "profile_sidebar_border_color": "3D0702",
        "profile_sidebar_fill_color": "2C1703",
        "profile_text_color": "A16A23",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 0,
      "favorite_count": 0,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Mon Jun 12 05:29:50 +0000 2017",
      "id": 874136640901480400,
      "id_str": "874136640901480448",
      "text": "#Liveperson $LPSN #stock  #investing Lower demand led to lower revenues and higher losses, in the first quarter company  losses widen  to $…",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "Liveperson",
            "indices": [
              0,
              11
            ]
          },
          {
            "text": "stock",
            "indices": [
              18,
              24
            ]
          },
          {
            "text": "investing",
            "indices": [
              26,
              36
            ]
          }
        ],
        "symbols": [
          {
            "text": "LPSN",
            "indices": [
              12,
              17
            ]
          }
        ],
        "user_mentions": [],
        "urls": []
      },
      "metadata": {
        "iso_language_code": "en",
        "result_type": "recent"
      },
      "source": "<a href=\"https://ifttt.com\" rel=\"nofollow\">IFTTT</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 1559920790,
        "id_str": "1559920790",
        "name": "CSIMarket.com",
        "screen_name": "CSIMarket",
        "location": "",
        "description": "A Financial Intelligence Company",
        "url": "http://t.co/jTuXpUQlA1",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/jTuXpUQlA1",
                "expanded_url": "http://csimarket.com",
                "display_url": "csimarket.com",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 808,
        "friends_count": 721,
        "listed_count": 40,
        "created_at": "Mon Jul 01 07:31:49 +0000 2013",
        "favourites_count": 32,
        "utc_offset": -18000,
        "time_zone": "Central Time (US & Canada)",
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 21695,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/844506254978502657/yPjreNts_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/844506254978502657/yPjreNts_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/1559920790/1418758808",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 0,
      "favorite_count": 0,
      "favorited": false,
      "retweeted": false,
      "lang": "en"
    },
    {
      "created_at": "Sun Jun 11 09:37:19 +0000 2017",
      "id": 873836532469289000,
      "id_str": "873836532469288965",
      "text": "#iMessage Nedir https://t.co/jODl3RseFB @tujigg #tujigg #apple #applepay #businesschat #facebook #genesys #ios11 #kobi #liveperson #nuance",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "iMessage",
            "indices": [
              0,
              9
            ]
          },
          {
            "text": "tujigg",
            "indices": [
              48,
              55
            ]
          },
          {
            "text": "apple",
            "indices": [
              56,
              62
            ]
          },
          {
            "text": "applepay",
            "indices": [
              63,
              72
            ]
          },
          {
            "text": "businesschat",
            "indices": [
              73,
              86
            ]
          },
          {
            "text": "facebook",
            "indices": [
              87,
              96
            ]
          },
          {
            "text": "genesys",
            "indices": [
              97,
              105
            ]
          },
          {
            "text": "ios11",
            "indices": [
              106,
              112
            ]
          },
          {
            "text": "kobi",
            "indices": [
              113,
              118
            ]
          },
          {
            "text": "liveperson",
            "indices": [
              119,
              130
            ]
          },
          {
            "text": "nuance",
            "indices": [
              131,
              138
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "tujigg",
            "name": "GÖKHAN GÖZEN",
            "id": 140474731,
            "id_str": "140474731",
            "indices": [
              40,
              47
            ]
          }
        ],
        "urls": [
          {
            "url": "https://t.co/jODl3RseFB",
            "expanded_url": "https://tujigg.wordpress.com/2017/06/11/imessage-nedir/",
            "display_url": "tujigg.wordpress.com/2017/06/11/ime…",
            "indices": [
              16,
              39
            ]
          }
        ]
      },
      "metadata": {
        "result_type": "recent",
        "iso_language_code": "tr"
      },
      "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 140474731,
        "id_str": "140474731",
        "name": "GÖKHAN GÖZEN",
        "screen_name": "tujigg",
        "location": "İstanbul, Türkiye",
        "description": "Superuser Level 7 @4sqturkiye @SwarmApp. Enterprise Projects Team Manager @YellowPagesTr.  Gizli şair. İzollu.",
        "url": "https://t.co/qLImCetoBR",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "https://t.co/qLImCetoBR",
                "expanded_url": "https://goo.gl/8UUnr4",
                "display_url": "goo.gl/8UUnr4",
                "indices": [
                  0,
                  23
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 81,
        "friends_count": 164,
        "listed_count": 14,
        "created_at": "Wed May 05 15:46:47 +0000 2010",
        "favourites_count": 254,
        "utc_offset": 10800,
        "time_zone": "Istanbul",
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 2985,
        "lang": "tr",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/497723467639160832/AdCILkU4.jpeg",
        "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/497723467639160832/AdCILkU4.jpeg",
        "profile_background_tile": true,
        "profile_image_url": "http://pbs.twimg.com/profile_images/867405480238485505/AHVQYXtz_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/867405480238485505/AHVQYXtz_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/140474731/1452457598",
        "profile_link_color": "0084B4",
        "profile_sidebar_border_color": "FFFFFF",
        "profile_sidebar_fill_color": "08FF18",
        "profile_text_color": "030303",
        "profile_use_background_image": true,
        "has_extended_profile": true,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "regular"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 0,
      "favorite_count": 0,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": true,
      "lang": "tr"
    },
    {
      "created_at": "Wed Jun 07 09:58:01 +0000 2017",
      "id": 872392193054584800,
      "id_str": "872392193054584832",
      "text": "LivePerson - Expert Advice: 25 % Off Your First Sessions. No Obligation. #LivePerson… https://t.co/2m4G6D9jcB",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "LivePerson",
            "indices": [
              73,
              84
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [],
        "urls": [
          {
            "url": "https://t.co/2m4G6D9jcB",
            "expanded_url": "https://goo.gl/fb/DV022f",
            "display_url": "goo.gl/fb/DV022f",
            "indices": [
              86,
              109
            ]
          }
        ]
      },
      "metadata": {
        "iso_language_code": "en",
        "result_type": "recent"
      },
      "source": "<a href=\"http://www.google.com/\" rel=\"nofollow\">Google</a>",
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 181342308,
        "id_str": "181342308",
        "name": "Best Coupon Online",
        "screen_name": "best_coupon",
        "location": "NY",
        "description": "Best Coupon Online: You can find and share coupon codes, promo codes, Free Shipping Codes for great discounts at thousands of online stores.",
        "url": "http://t.co/rLKSXUDkEZ",
        "entities": {
          "url": {
            "urls": [
              {
                "url": "http://t.co/rLKSXUDkEZ",
                "expanded_url": "http://www.bestcoupononline.com",
                "display_url": "bestcoupononline.com",
                "indices": [
                  0,
                  22
                ]
              }
            ]
          },
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 2616,
        "friends_count": 1619,
        "listed_count": 209,
        "created_at": "Sat Aug 21 23:32:23 +0000 2010",
        "favourites_count": 1,
        "utc_offset": -36000,
        "time_zone": "Hawaii",
        "geo_enabled": false,
        "verified": false,
        "statuses_count": 92523,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/138438687/bestcoupononline_-_bag1.gif",
        "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/138438687/bestcoupononline_-_bag1.gif",
        "profile_background_tile": true,
        "profile_image_url": "http://pbs.twimg.com/profile_images/1108094782/BCO-1_normal.gif",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/1108094782/BCO-1_normal.gif",
        "profile_link_color": "0084B4",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 0,
      "favorite_count": 0,
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "lang": "en"
    },
    {
      "created_at": "Mon Jun 05 23:30:48 +0000 2017",
      "id": 871871961135292400,
      "id_str": "871871961135292416",
      "text": "@ZAID_ALZOUBI @al_3jmi93 اظن هذا اول شي تعلمتوه بالجامعة 😂 \n#liveperson",
      "truncated": false,
      "entities": {
        "hashtags": [
          {
            "text": "liveperson",
            "indices": [
              60,
              71
            ]
          }
        ],
        "symbols": [],
        "user_mentions": [
          {
            "screen_name": "ZAID_ALZOUBI",
            "name": "ابو راضي | زيد",
            "id": 616363446,
            "id_str": "616363446",
            "indices": [
              0,
              13
            ]
          },
          {
            "screen_name": "al_3jmi93",
            "name": "KH.Al3jmi",
            "id": 919144776,
            "id_str": "919144776",
            "indices": [
              14,
              24
            ]
          }
        ],
        "urls": []
      },
      "metadata": {
        "iso_language_code": "ar",
        "result_type": "recent"
      },
      "source": "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
      "in_reply_to_status_id": 871871719429898200,
      "in_reply_to_status_id_str": "871871719429898240",
      "in_reply_to_user_id": 616363446,
      "in_reply_to_user_id_str": "616363446",
      "in_reply_to_screen_name": "ZAID_ALZOUBI",
      "user": {
        "id": 268106376,
        "id_str": "268106376",
        "name": "أحمد الظفيري",
        "screen_name": "Aldahfeeri",
        "location": "Cookeville, TN  / Aljahra, Q8",
        "description": "وسارعوا إلى مغفرة من ربكم وجنة عرضها السماوات والأرض أعدت للمتقين. Chemical Engineer / Snap / Insta : prince_q8",
        "url": null,
        "entities": {
          "description": {
            "urls": []
          }
        },
        "protected": false,
        "followers_count": 709,
        "friends_count": 257,
        "listed_count": 5,
        "created_at": "Fri Mar 18 04:33:40 +0000 2011",
        "favourites_count": 1127,
        "utc_offset": -18000,
        "time_zone": "Quito",
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 17085,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "C0DEED",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/784100141250781192/brXGu4fX_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/784100141250781192/brXGu4fX_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/268106376/1493138090",
        "profile_link_color": "1DA1F2",
        "profile_sidebar_border_color": "C0DEED",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": true,
        "default_profile": true,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false,
        "translator_type": "none"
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "retweet_count": 0,
      "favorite_count": 0,
      "favorited": false,
      "retweeted": false,
      "lang": "ar"
    }
  ],
  "search_metadata": {
    "completed_in": 0.027,
    "max_id": 874444190968668200,
    "max_id_str": "874444190968668160",
    "query": "%23liveperson",
    "refresh_url": "?since_id=874444190968668160&q=%23liveperson&include_entities=1",
    "count": 10,
    "since_id": 0,
    "since_id_str": "0"
  }
}
module.exports = _.extend({
    sequelize: sequelize,
    Sequelize: Sequelize,
    sample_response:response
}, db);