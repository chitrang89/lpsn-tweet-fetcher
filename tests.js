var expect = require("chai").expect;
var server = require("../server/server/index");
var repository = require("../server/server/repository").repository.prototype;
var chai = require("chai");
var chaiHttp = require("chai-http");
var db = require('../server/server/models');
chai.use(chaiHttp);
var should = chai.should();


describe("LivePerson - Tweet Fetcher", function () {

    it("should return zero", function () {
        chai.request(server).get('/test').end(function (err, res) {
            res.should.have.status(200);
            expect(res.body).to.equal(0);
        });
    });

    it("should search and fetch 2 tweets from Twitter for a given keyword from twitter", function () {
        chai.request(server).get('/api/tweets/liveperson/false').end(function (err, res) {
            res.should.have.status(200);
            expect(res.body.statuses.length).to.be.greaterThan(0);
        });
    });

    it("should return row count to be greater than 0", function () {
        return Promise.resolve(repository.returnAll(db)).then(function (results) {
            expect(results.length).to.be.greaterThan(0);
        });

    });

    it("should only search and fetch tweets from Twitter for keyword liveperson", function () {
        var currentDbCount = 0
        return Promise.resolve(repository.returnAll(db))
            .then(function (results) {
                currentDbCount = results.length;
                return chai.request(server).get('/api/tweets/liveperson/false')
            }).then(function (err, res) {
                return Promise.resolve(repository.returnAll(db));
            }).then(function (results) {
                var newCount = results.length;
                expect(newCount).to.equal(currentDbCount);
            });

    });

    it.skip("should search,save and fetch tweets from Twitter for keyword liveperson", function () {

        var currentDbCount = 0
        return Promise.resolve(repository.returnAll(db))
            .then(function (results) {
                currentDbCount = results.length;
                return chai.request(server).get('/api/tweets/amithabh/true')
            }).then(function (err, res) {
                return Promise.resolve(repository.returnAll(db));
            }).then(function (results) {
                var newCount = results.length;
                expect(newCount).to.equal(currentDbCount);
            });
    });

    it("should retrieve no results from db for an incorrect tweet id", function () {
        return Promise.resolve(repository.findByTweetId('8718718961135292416', db))
            .then(function (results) {
                expect(results).not.equal(0);
            })
            .catch(function (err) {
                throw err;
            })
    });

    it("should retrieve 2 results from filter method for provided tweet ids", function () {
        var idsToFind = ["871871961135292416", "872392193054584832"];
        var fakeData = [{
            TweetId: "874444190968668160"
        }, {
            TweetId: "874375269964021760"
        }, {
            TweetId: "874311367431344128"
        }, {
            TweetId: "874136640901480448"
        }, {
            TweetId: "873836532469288965"
        }, {
            TweetId: "872392193054584832"
        }, {
            TweetId: "871871961135292416"
        }];

        return Promise.resolve(repository.filterIdsFromArray(idsToFind,
            fakeData))
            .then(function (results) {
                expect(results.length).to.equal(2);
            })
            .catch(function (err) {
                throw err;
            })
    });
});

