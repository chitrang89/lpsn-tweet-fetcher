const fs = require("fs");
const Sequelize = require('./node_modules/sequelize');
const _ = require('underscore');
function repository() {

}

/**
 * Responsible for fetching and returing all tweets in the databse
 */
repository.prototype.returnAll = function (db) {
    return db['SearchObject'].findAll({
        attributes: ['TweetsObject']
    }).then(results => {
        return results;
    })
}

/**
 * Responsible for directly querying the database for a tweet by TweetId
 */
repository.prototype.findByTweetId = function (id, db) {
    return db['SearchObject'].findAll({
        where: {
            TweetId: id
        }
    }).then(results => {
        return results
    });
}

/**
 * Responsible for quering and processing all data from the database
 */
repository.prototype.findByTweetIdArray = function (ids, db) {
    return db['SearchObject'].findAll({
        attributes: ['TweetsObject', 'TweetId']
    }).then(results => {
        return repository.prototype.filterIdsFromArray(ids, results);
    })
}

/**
 * Responsible for filtering common ids found in search data
 */
repository.prototype.filterIdsFromArray = function filterIdsFromArray(ids, data) {
    var found = [];
    _.each(data, function (entry) {
        if (_.contains(ids, entry.TweetId)) {
            found.push(entry.TweetId);
        }
    });

    return found;
}

/**
 * Responsible for fetching and processing data from twitter 
 */
repository.prototype.getTweetsAndSave = function (req, res, client, db) {
    var shouldSave = req.params.shouldSave;
    var params = {
        q: '#' + req.params.query,
        count: 10
    };
    client.get('search/tweets.json', params, function (error, tweets, response) {
        if (!error) {
            if (shouldSave === 'true') {
                ProcessData(db, tweets);
            }
            res.send(tweets);
        }
        else {
            res.send(500, error)
        }
    });
}

/**
 * Creates a new entry into the database
 */
function createEntry(tweet, db) {
    var Model = db['SearchObject'];
    Model.create({
        TweetsObject: JSON.stringify(tweet),
        TweetId: tweet.id_str
    });

}

/**
 * Responsible for processing data to be saved to the database.
 */
function ProcessData(db, tweets) {
    return repository.prototype.findByTweetIdArray(tweets.statuses, db).then(results => {
        if (results.length < 1) // confirms these tweets are not in db at this point so we save them all
        {
            _.each(tweets.statuses, function (tweet) {
                createEntry(tweet, db);
            })
        }

        if (results.length > 0) {
            _.each(tweets.statuses, function (tweet) {
                if (_.contains(results, tweet.id_str)) { // only save the new ones
                    createEntry(tweet, db);
                }
            })
        }
    })
}


module.exports.repository = repository;