const express = require('express');
const bodyParser = require('body-parser');
const fs = require("fs");
var Twitter = require('twitter');
var env = require('node-env-file');
var model = require('./SearchModel');
var repository = require('./repository').repository.prototype;
const sequelize = require('./node_modules/sequelize');
var gzippo = require('gzippo');
var path = require('path');
const file = "data.db";
const limit = 10;
var db = require('./models');

env(__dirname + '/.env');
var client = new Twitter({
    consumer_key: process.env.consumer_key,
    consumer_secret: process.env.consumer_secret,
    access_token_key: process.env.access_token_key,
    access_token_secret: process.env.access_token_secret
});

// API CONFIGURATION
var app = express();
app.use(gzippo.staticGzip("" + __dirname + "/client"));
app.get('/', function(req, res) {
    res.sendFile(path.join(path.resolve(__dirname) + '/client/index.html'));
});
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, PUT, DELETE, PATCH, POST');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json()); // for parsing application/json
db.sequelize.sync().then(function () {
    app.listen(process.env.PORT ||9000, function () {
        console.log('listening on '+this.address().port)
    });
});

app.get('/api/tweets/:query/:shouldSave', (req, res) => {

    return repository.getTweetsAndSave(req, res, client, db);


});



module.exports = app;