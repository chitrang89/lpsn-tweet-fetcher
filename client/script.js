angular.module('ListTest', ['ngMaterial'])
  .controller('AppCtrl', function ($scope, $http, $location) {
    $scope.keyword = '#';
    $scope.todos = [];
    $scope.tweets = [];

    
    $scope.getTweets = function (shouldSave) {
      $scope.keyword = $scope.process($scope.keyword);
      $http({ method: 'GET', url: $location.path() + '/api/tweets/' + $scope.keyword + '/' + shouldSave })
        .success(function (data, status) {
          $scope.tweets = [];
          for (i = 0; i < data.statuses.length; i++) {
            var tweet = data.statuses[i];
            $scope.tweets.push({
              what: tweet.text,
              who: tweet.user.name,
              notes: tweet.user.description
            })
          }
        })

        .error(function (data, status) {
          $scope.status = status;
          $scope.data = "Request failed";
        });
    };

/**
 * Remove # and any white space
 */
    $scope.process=function (input)
    {
        if(input.indexOf('#')>-1)
            input = input.replace('#','');

        return input.replace(/\s/g,'');
    }
  });