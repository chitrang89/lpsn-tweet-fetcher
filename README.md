# README #

### How do I get set up? ###
- Clone this repository, run ```npm install``` and then ```npm start```

- Run ```npm test``` to run unit tests.

- If you get an error about sqlite3, it will most likely because it needs to install it globally. Run ```npm i sqlite3 --build-from-source``` to fix. If this fails, you will need to update your npm by running ```sudo npm install -g npm```

- You can use the API by sending ```GET``` request to ```http://localhost:{port}/api/tweets/{keyword}/{shouldSave?}```

-> ```{port}``` will be set to 9000 by default unless the process assigns otherwise. You shall see the port as part of ```Listening on PORT NUMBER``` message when you run ```npm start```

-> ```{keyword}``` can simply be the text you wish to search by. The API then searches Twitter for tweets with ```#{keyword}``` and returns results.

-> ```{shouldSave}``` is simply a boolean (true/false) to let API know whether to save the tweets or not.